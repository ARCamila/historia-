
package historiapoo;

public class HistoriaPOO {

   
    public static void main(String[] args) {
        Hombre Esteban;
        Esteban=new Hombre();
        Perro PerrosSec;
        PerrosSec= new Perro();
        SonidosExternos Calle;
        Calle=new SonidosExternos();
        Mujer Carolina;
        Carolina=new Mujer();
        Esteban.nombre=("Esteban");
        Carolina.nombre=("Carolina");
        
        System.out.println("Como era ya costumbre a las 4:30 de la tarde de todos los martes, se encontraba  puntualmente sentado en la banca frente a la fuente del parque pasando la avenida, llovía"
                + " y el sonido de las gotas golpeando el piso se confundía con la melodía de su reproductor de música, haciendo eco en su cabeza:  ");
        
        for(int i=0;i<5;i++){
           Esteban.llover();
        }
        
        System.out.println("“It's been a hard day's night, and I've been working like a dog”");
        System.out.println("Veía pasar gente con apuro junto a sus paraguas.");
        System.out.println("Escuchaba los ladridos de los perros que jugaban a traer la pelota.");
        
        for(int i=0;i<5;i++){
            PerrosSec.ladrar();
        }
        
        System.out.println("Y disfrutaba como de a poco las luces de la ciudad tomaban el protagonismo.");
        System.out.println("Todo parecía mantener el curso de las cosas, hasta que escuchó un pequeño estruendo a lo lejos, lo más parecido a un charco, en su impresión: ");
        
        Carolina.estruendo();
        Carolina.exclamar();
        
        System.out.println("Aun queriendo evadirlo, no puedo evitar voltear a ver y encontrarse con una expresión de preocupación en el rostro de una mujer que recogía un libro con delicadeza y angustia a la vez, al intentar pasar sus páginas y evaluar el daño sufrido");
        Esteban.exclamar();
        
        System.out.println("Su bicicleta estaba ligeramente recostada contra uno de los postes junto con todas sus pertenencias, que hicieron más peso y no puedo evitar que se ladeara y cayera");
        Carolina.caer();
        Esteban.exclamar2();
        
        System.out.println("Exclamó un par de veces, sin obtener respuesta.");
        System.out.println("Era ya la canción número 45 de su reproductor de música y se escuchaba:");
        System.out.println("“You ain't nothin' but a hound dog, cryin' all the time”");
        System.out.println("Por primera vez en mucho tiempo tuvo la iniciativa de irrumpir su realidad,asì que se levantó y fue hasta el lugar donde se encontraba la bicicleta");
        System.out.println("“Oye, tu bicicleta se ha caìdo junto con tus cosas, la dejè recostada en este lugar”");
        System.out.println("“Sì, sì, agradezco tu amabilidad”.  Dijo apresuradamente.");
        System.out.println("“Fue mucho el daño?”");
        System.out.println("“Ah, sí, siendo honesta, hay bastantes hojas dañadas, tal vez sean daños irreparables”.");
        System.out.println("“Lamento escuchar eso”.");
        for(int i=0;i<3;i++){
            Carolina.quejarse();
        }
        System.out.println("“No, no te preocupes, gracias por ayudarme con la bicicleta, probablemente hubiera sido màs grave la situación”.");
        System.out.println("“Bueno, que tengas buena noche” le dijo mientras se alejaba lentamente junto con su bicicleta y libro bajo el brazo.");
        System.out.println("Por suerte había dejado de llover y por un momento toda su atención estuvo puesta en una sola cosa: ");
        System.out.println(", ese libro que se alejaba bajo el brazo de aquella muchacha, a pesar del sonido del tráfico que se escuchaba tras el telón de esta mágica escena que acababa de contemplar.");
        for(int i=0; i<10; i++){
           Calle.pitar(); 
        }
        System.out.println("“How does it feel, to be on your own, with no direction homeee”");
        for(int i=0; i<10; i++){
           Calle.pitar(); 
        }
        System.out.println("Justo antes de llegar al semáforo, mientras se disponía a ver las luces de colores y esperar el verde, escuchó que de lejos alguien intentaba llamar su atención.");
        for(int i=0; i<4; i++){
           Esteban.gritar();
        }
        System.out.println("Era èl que llegaba corriendo mientras intentaba decirle algo entre dientes.");
        System.out.println("“Jajajaja,espera, no te entiendo nada” le dijo.");
        System.out.println("Lo siento, es que creí que el semáforo cambiaría");
        System.out.println("Bueno, lo que quería decir, era que conozco de un amigo que sabe cómo reparar libros con ese tipo de daños");
        System.out.println("Así que si quisieras podrías acompañarme por un rato y mañana de seguro tu libro estaría siendo reparado");
        System.out.println("Ella se quedó viéndolo por par segundos hasta que para el alivio de él se asomó un sí");
        System.out.println("En su cabeza sonaba: “Here comes the sun (doo doo doo doo)”");
        System.out.println("Caminaron un par de manzanas mientras los demás eran testigos de las risas que iban y venían");
        for(int i=0; i<2;i++){
            Esteban.reir();
        }
        for(int i=0; i<2;i++){
            Carolina.reir();
        }
        System.out.println("Finalmente entraron a un lugar que para su sorpresa permanecía abierto, donde durante toda la noche junto al sarcasmo, ironía y las luces de la ciudad hablaron sobre aquel libro y miles de historias más");
        System.out.println("Que sólo tendría se vería obligada a terminar cuando el sol se asomó por la ventana");
        System.out.println("Caminando, regresaron al parque y se sentaron en la banca frente a la fuente");
        System.out.println("y como quien no quiere irse hubo un diminuto silencio a la espera de una negativa o de algo que no los obligara a seguir con el curso de las cosas, pero, eso no llegó, la realidad no se lo permitía.");
        System.out.println("La inesperada despedida se hizo llegar y de manera rápida como para no arrepentirse estrecharon sus manos mientras se decían sus nombres");
        System.out.println("Cosa que sorprendentemente no tuvo lugar durante toda la noche");
        System.out.println("Esteban");
        System.out.println("Carolina");
        System.out.println("“Bueno, es hora de irme” replicó ella");
        System.out.println("Con lo cual se despidieron y prometieron contactarse de nuevo con el motivo del libro reparado");
        System.out.println("Cosa que a ciencia cierta no superaba la expectativa de volverse a ver e interrumpir su realidad, tal vez el verdadero motivo");
        System.out.println("Así la vio alejarse, pero esta vez sin correr tras de ella, mientras sus pasos armonizaban con una versión del 58’: ");
        for(int i=0; i<2; i++){
            Calle.tararear();
        }
        System.out.println("“When you're smilin'...keep on smilin', the whole world smiles with you”");
        System.out.println("Esperando ansiosamente la próxima vez en que inesperadamente cambiara el curso de las cosas.");
       
       
      
        
        
      
       
    }
    
}
